FROM node:slim
WORKDIR /var/lib/jenkins/workspace/docker
COPY . /var/lib/jenkins/workspace/docker/
RUN npm install
CMD node server.js
EXPOSE 1337
